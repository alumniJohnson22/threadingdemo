package com.m3.training.threadingdemo;

public class ConsumerOfTicker implements Runnable {
	private int count = 100;

	private Ticker ticker;

	// read stock price

	public ConsumerOfTicker(Ticker ticker) {
		this.ticker = ticker;
	}

	@Override
	public void run() {
		for (int index = 0; index < count; index++) {
			synchronized (ticker) {
				try {
					ticker.wait();
					System.out.println(Thread.currentThread().getName() + " reading price " + ticker.getPrice());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

}
