package com.m3.training.threadingdemo;

public class Ticker {
	
	private double price;
	private String symbol = "BYND";

	public Ticker() {
		this.setPrice(-0.01);
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
