package com.m3.training.threadingdemo;

import java.util.ArrayList;
import java.util.List;

public class FlightTicketsNarrowLock implements Runnable {
	
	private List<Integer> seats = new ArrayList<>();
	private static final int MAX_SEATS = 25;
	private Integer count = 0;

	public FlightTicketsNarrowLock() {
		for (int index=0; index< MAX_SEATS; index++) {
			seats.add(0);
		}
	}

	@Override
	public void run() {
		while (count < MAX_SEATS) {

			boolean soldASeat = false;
			int seatNumber = -1;
			synchronized(count) {
				int nextSeat = count;
				int marker = seats.get(nextSeat);
				if (marker == 0) {
					seats.set(nextSeat, ++marker);
					count++;
					seatNumber = nextSeat;
					soldASeat = true;
				}
			}
			if (soldASeat) {
				System.out.println(Thread.currentThread().getName() + " sold seat " + seatNumber);
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		
		FlightTicketsNarrowLock flight = new FlightTicketsNarrowLock();
		Thread worker1 = new Thread(flight);
		Thread worker2 = new Thread(flight);
		worker1.setName("worker 1");
		worker2.setName("worker 2");
		
		worker1.start();
		worker2.start();
		
		worker1.join();
		worker2.join();
		
		for (int index = 0; index< flight.seats.size(); index++) {
			System.out.println("At seat " + index + " sold: " + flight.seats.get(index));
		}
		
		
	}
}
