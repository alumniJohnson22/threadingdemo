package com.m3.training.threadingdemo;

import java.util.Random;

public class Naive extends Thread {
	
	private int someResult;
	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName());
		someResult = new Random().nextInt(3);
		System.out.println(Thread.currentThread().getName() + " exits");			
	}

	public static void main(String[] args) throws InterruptedException {
		Thread.currentThread().setName("parent");		
		System.out.println(Thread.currentThread().getName());	
		Naive childThread = new Naive();
		childThread.setName("Child");
		childThread.start();
		childThread.join();
		System.out.println(childThread.someResult);
		System.out.println(Thread.currentThread().getName() + " exits");			
	}

}
