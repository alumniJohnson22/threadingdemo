package com.m3.training.threadingdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorkWithArray implements Runnable {
	
	private static final int MAX = 100;
	private List<Integer> list = new ArrayList<>();
	int listCurrentIndex;
	
	@Override
	public void run() {
		//printList(populateList());
		printListHeapIndex(populateList());
	}
	
	private List<Integer> populateList() {
		Random random = new Random();
		for (int index = 0 ; index < MAX; index++) {
			list.add(random.nextInt(100));
		}
		return list;
	}
	
	private void printList(List<Integer> list) {
		for (int index = 0; index < list.size(); index++) {
			Integer current = list.get(index);
			System.out.println(
				Thread.currentThread().getName() 
				+ " list count " + index + " value " + current);
		}
	}
	
	private void printListHeapIndex(List<Integer> list) {
		for (listCurrentIndex = 0; listCurrentIndex < list.size(); listCurrentIndex++) {
			Integer current = list.get(listCurrentIndex);
			System.out.println(
				Thread.currentThread().getName() 
				+ " list count " + listCurrentIndex + " value " + current);
		}
	}
	
	public static void main(String[] args) {
		WorkWithArray job;
		WorkWithArray job1 = new WorkWithArray();
		job = job1;
		Thread worker1 = new Thread(job);
		worker1.setName("Child1");
		
		//WorkWithArray job2 = new WorkWithArray();
		//job = job2;
		Thread worker2 = new Thread(job);
		worker2.setName("Child2");
		
		worker1.start();
		worker2.start();
		System.out.println(Thread.currentThread().getName() + " exits");
	}

}
