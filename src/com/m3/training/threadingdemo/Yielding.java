package com.m3.training.threadingdemo;

public class Yielding implements Runnable {
	
	private static final int MAX = 100;

	@Override
	public void run() {
		
		for (int index = 0; index < MAX; index++) {
			System.out.println(Thread.currentThread().getName() + " yielding.");
			Thread.yield();
			System.out.println(Thread.currentThread().getName() + " back to work");
		}
	}

	public static void main(String[] args) {
		Yielding job = new Yielding();
		Thread worker1 = new Thread(job);
		Thread worker2 = new Thread(job);
		Thread worker3 = new Thread(job);
		
		worker1.start();
		worker2.start();
		worker3.start();
		
	}
	
}
