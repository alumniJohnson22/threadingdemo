package com.m3.training.threadingdemo;

import java.util.ArrayList;
import java.util.List;

public class FlightTicketsTooBroadLock implements Runnable {
	
	private List<Integer> seats = new ArrayList<>();
	private static final int MAX_SEATS = 25;
	private int count = 0;

	public FlightTicketsTooBroadLock() {
		for (int index=0; index< MAX_SEATS; index++) {
			seats.add(0);
		}
	}

	@Override
	public synchronized void run() {
		while (count < MAX_SEATS) {
			int nextSeat = count;
			int marker = seats.get(nextSeat);
			seats.set(nextSeat, ++marker);
			System.out.println(Thread.currentThread().getName());
			count++;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		
		FlightTicketsTooBroadLock flight = new FlightTicketsTooBroadLock();
		Thread worker1 = new Thread(flight);
		Thread worker2 = new Thread(flight);
		worker1.setName("worker 1");
		worker2.setName("worker 2");
		
		worker1.start();
		worker2.start();
		
		worker1.join();
		worker2.join();
		
		for (int index = 0; index< flight.seats.size(); index++) {
			System.out.println("At seat " + index + " sold: " + flight.seats.get(index));
		}
		
		
	}
}
