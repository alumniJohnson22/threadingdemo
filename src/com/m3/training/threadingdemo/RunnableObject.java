package com.m3.training.threadingdemo;

public class RunnableObject implements Runnable {


	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getName() + " exits");
	}
	
	public static void main(String[] args) {
		Thread.currentThread().setName("parent");		
		System.out.println(Thread.currentThread().getName());	
		RunnableObject obj = new RunnableObject();
		Thread thread = new Thread(obj);
		thread.setName("Child");
		thread.start();
		System.out.println(Thread.currentThread().getName() + " exits");			
	}
}
