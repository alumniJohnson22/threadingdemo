package com.m3.training.threadingdemo;

import java.util.Random;

public class ProducerOfTicker implements Runnable {
	private int count = 100;
	private Ticker ticker;

	// update stock price

	public ProducerOfTicker(Ticker ticker) {
		this.ticker = ticker;
	}
	
	@Override
	public void run() {
		for (int index = 0; index < count; index++) {
			double price = Math.random() * 10;
			synchronized(ticker) {
				ticker.setPrice(price);
				ticker.notify();
			}
			System.out.println(Thread.currentThread().getName() + " set price " + ticker.getPrice());	
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
