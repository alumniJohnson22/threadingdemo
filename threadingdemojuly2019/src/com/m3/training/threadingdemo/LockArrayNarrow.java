package com.m3.training.threadingdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LockArrayNarrow implements Runnable {
	private static final int MAX = 100;
	List<Integer> list = new ArrayList<>();
	
	@Override
	public void run() {
		printList(populateList());
	}
	
	private List<Integer> populateList() {
		Random random = new Random();

		for (int index = 0 ; index < MAX; index++) {
			synchronized(list) {
				list.add(random.nextInt(100));
			}
			System.out.println(Thread.currentThread().getName() 
				+ " populate " + index);
		}
		return list;
	}
	
	private  void printList(List<Integer> list) {
		for (int index = 0; index < list.size(); index++) {
			Integer current = list.get(index);
			System.out.println(
				Thread.currentThread().getName() 
				+ " list count " + index + " value " + current);
		}
	}
	
	public static void main(String[] args) {
		LockArrayNarrow job1 = new LockArrayNarrow();
		
		Thread worker1 = new Thread(job1);
		worker1.setName("Child1");
	
		Thread worker2 = new Thread(job1);
		worker2.setName("Child2");
		
		worker1.start();
		worker2.start();
	}

}
