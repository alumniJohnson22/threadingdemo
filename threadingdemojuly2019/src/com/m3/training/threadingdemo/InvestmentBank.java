package com.m3.training.threadingdemo;

public class InvestmentBank {
	
	public static void main(String[] args) {
		
		Ticker ticker = new Ticker();
		ProducerOfTicker producer = new ProducerOfTicker(ticker);
		ConsumerOfTicker consumer = new ConsumerOfTicker(ticker);
		Thread workerProducing = new Thread(producer);
		workerProducing.setName("BB feed");
		workerProducing.start();
		Thread workerConsuming = new Thread(consumer);
		workerConsuming.setName("TraderJoe");
		workerConsuming.start();
		Thread workerConsuming2 = new Thread(consumer);
		workerConsuming2.setName("TraderBro");
		workerConsuming2.start();
	}

}
